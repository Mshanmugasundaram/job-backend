const express = require('express');
const cors = require('cors');
const app = express();
const passport = require('./services/passport');

require('dotenv').config({ path: __dirname + '/.env' });

const db = require('./config/db');

app.use(passport.initialize());
app.use(passport.session());
// app.use('/uploads', express.static('uploads'))
app.use(cors());
app.use(express.json());
const Router = require('./routes');
Router(app);

app.use(({ statusCode, apiStatus, data = {}, errorMessage }, req, res, next) => {
	// console.log(apiStatus, statusCode, data, errorMessage);
	res.status(statusCode || 200).json({
		apiStatus,
		data,
		meta: {
			message: errorMessage,
		},
	});
});

const PORT = 7000;

app.listen(PORT, () => {
	console.log(`Server running at PORT: ${PORT}`);
});

module.exports = app;
