const Company = require('../models/companySchema')
const Recruiter = require('../models/recruiterSchema')


const addCompany = async(req, res, next) => {
    let {name, address, type, email, description} = req.body
    try {
        if(req.user.role != 'owner') {
            return next({apiStatus: "FAILURE", errorMessage: "ACCESS_DENIED", statusCode: 403})
        }
        let company = await Company.findOne({email: email})
        if(company != null) {
            await company.updateOne({
                name,
                address,
                description,
                email,
                type
            })
            return next({apiStatus: "SUCCESS", data: company, statusCode: 200})

        }
        let newCompany = new Company({
            name,
            owner: req.user._id,
            email,
            description,
            address,
            type
        })
        await newCompany.save()
        return next({apiStatus: "SUCCESS", data: newCompany, statusCode: 200})
    } catch(err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}

const recruiterList = async(req, res, next) => {
    try {
        let company = await Company.findOne({owner: req.user._id})
        console.log(company)
        if(company == null) {
            return next({apiStatus: "FAILURE", errorMessage: "COMPANY_DOESN'T_EXISTS", statusCode: 404})
        }
        let recruiters = await Recruiter.find({companyId: company._id}, 'userId companyId isVerified')
        return next({apiStatus: "SUCCESS", data: recruiters, statusCode: 200})
    } catch(err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}

const companyDetails = async(req, res, next) => {
    let company = await Company.findOne({owner: req.user._id})
    return next({apiStatus: "SUCCESS", data: company, statusCode: 200})
}

const companies = async(req, res, next) => {
    let company = await Company.find({});
    return next({apiStatus: "SUCCESS", data: company, statusCode: 200})

}


module.exports = {
    addCompany,
    recruiterList,
    companyDetails,
    companies
}