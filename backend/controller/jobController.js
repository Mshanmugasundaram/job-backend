const Job = require('../models/jobSchema')
const Recruiter = require('../models/recruiterSchema')
const Company = require('../models/companySchema')
const UserProfile = require('../models/userProfileSchema')
const Skill = require('../models/skillSchema')

const addJob = async(req, res, next) => {
    let { name, description, location, salary, experience, skills, industry} = req.body
    console.log(req.body)
    try {
        if(req.user.role != 'recruiter') {
            return next({apiStatus: "FAILURE", errorMessage: "ACCESS_DENIED", statusCode: 403})
        }
        let recruiter = await Recruiter.findOne({userId: req.user._id})
        console.log(recruiter)
        if(recruiter.isVerified == false) {
            return next({apiStatus: "FAILURE", errorMessage: "PROFILE_NOT_VERIFIED_BY_COMPANY", statusCode: 403})

        }
        let skill = await Skill.exists({name: skills})
        if(!skill) {
            let newSkill = new Skill({
                name: skills
            })
            await newSkill.save()
        }
        // let user = await UserProfile.findOne({userId: req.user._id})
        // let company = await Company.findOne({_id: recruiter.companyId})
        let newJob = new Job({
            name,
            description,
            location,
            salary,
            experience,
            skills,
            industry,
            recruiterId: recruiter._id,
            companyId: recruiter.companyId
        })
        await newJob.save()
        return next({apiStatus: "SUCCESS", data: newJob
        , statusCode: 200})
    } catch(err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}

const listJobs = async(req, res, next) => {
    let jobList = []
    try {
    let jobs = await Job.find({})
    for(let i=0; i<jobs.length; i++) {
        let company = await Company.findOne({_id: jobs[i].companyId})
        let recruiter = await Recruiter.findOne({_id: jobs[i].recruiterId})
        console.log(recruiter.userId)
        let userProfile = await UserProfile.findOne({userId: recruiter.userId})
        console.log(userProfile.firstName)
        let job = {
            id: jobs[i]._id,
            company: company.name,
            skills: jobs[i].skills,
            location: jobs[i].location,
            description: jobs[i].description,
            job: jobs[i].name,
            salary: jobs[i].salary,
            experience: jobs[i].experience,
            industry: jobs[i].industry,
            recruiter: userProfile.firstName
        }
        jobList.push(job)
    }
    } catch(err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
    return next({apiStatus: "SUCCESS", data: jobList, statusCode: 200})
}

const selectJob = async(req, res, next) => {
    let {jobId} = req.params
    let selectedJob = await Job.findById(jobId)
    console.log(selectedJob)
    let company = await Company.findOne({_id: selectedJob.companyId})
    console.log(company)
    let recruiter = await Recruiter.findOne({_id: selectedJob.recruiterId})

    let userProfile = await UserProfile.findOne({userId: recruiter.userId})
    return next({apiStatus: "SUCCESS", data: {
        id: selectedJob._id,
        company: company.name,
        recruiter:userProfile.firstName,
        job: selectedJob.name,
        description: selectedJob.description,
        experience: selectedJob.experience,
        industry: selectedJob.industry,
        location: selectedJob.location,
        salary: selectedJob.salary,
        skills: selectedJob.skills
    }, statusCode: 200})
}

const applyJob = async(req, res, next) => {
    let {jobId} = req.params;
    let {userId} = req.body
    let job = await Job.findById(jobId);
    if( job == null) {
        return next({apiStatus: "FAILURE", errorMessage: "JOB_DOESN'T_EXISTS", statusCode: 404})
    }
    let userProfile = await UserProfile.findOne({userId: userId})
    await userProfile.updateOne({$push: {jobs: job._id}})
    return next({apiStatus: "SUCCESS", data: userProfile, statusCode: 200})
}

const applied = async(req, res, next) => {
    try {
        let jobDetails = [];
        let jobs = await UserProfile.findOne({userId: req.user._id}).select('jobs')
        // console.log(jobs.jobs)
        for(let i=0; i<jobs.jobs.length; i++) {
            let job = await Job.findOne({_id: jobs.jobs[i]})
            jobDetails.push(job)
        }
        console.log(jobDetails)
        return next({apiStatus: "SUCCESS", data: jobDetails, statusCode: 200})
    } catch(err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}

const editJob = async(req, res, next) => {
    let { name, description, location, salary, experience, skills, industry} = req.body
    let { jobId } = req.params
    try {
        let job = await Job.findOne({_id: jobId})
        console.log(job)
        if(job == null) {
            return next({apiStatus: "FAILURE", errorMessage: "JOB_DOESN'T_EXISTS", statusCode: 404})
        }
        await job.updateOne({
            name: name ? name : job.name,
            description: description ? description : job.description,
            location: location ? location : job.location,
            salary: salary ? salary : job.salary,
            experience: experience ? experience : job.experience,
            skills: skills ? skills : job.skills,
            industry: industry ? industry : job.industry,
        })
        return next({apiStatus: 'SUCCESS', data: "UPDATED_SUCCESSFULLY", statusCode: 200})
    } catch(err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}
module.exports = {
    addJob,
    listJobs,
    selectJob,
    applyJob,
    applied,
    editJob
}