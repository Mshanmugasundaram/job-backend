const Position = require("../models/positionSchema")

const addPosition = async(req, res, next) => {
    let { name, companyId } = req.body 
    try {
        if(req.user.role == recruiter) {
            let position = await Position.findOne({name: name, companyId: companyId})
            if(position != null) {
                return next({apiStatus: "FAILURE", errorMessage: "POSITION_ALREADY_EXISTS", statusCode: 409})
            }
            let newPosition = new Position({
                name,
                companyId,
                createdBy: req.user._id
            })
            await newPosition.save()
            return next({apiStatus: "SUCCESS", data: newPosition, statusCode: 200})
        }
    } catch(err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}

module.exports = {
    addPosition
}