const UserProfile = require('../models/userProfileSchema');
const User = require('../models/userSchema')
const Skill = require('../models/skillSchema')


const createUserProfile = async(req, res, next) => {
    let {userId, firstName, lastName, age, location, gender, experience, qualification, skill} = req.body
    let input = req.body
    let skills = []
    console.log(req.body)
    try {
        let userProfile = await UserProfile.findOne({userId: userId})
        console.log(userProfile)
        if(userProfile) {
            // return next({apiStatus: "FAILURE", errorMessage: "PROFILE_ALREADY_EXISTS", statusCode: 409})
            await userProfile.updateOne({
                firstName: input.firstName ? input.firstName : userProfile.firstName,
                lastName: input.lastName ? input.lastName : userProfile.lastName,
                age: input.age ? input.age : userProfile.age,
                experience: input.experience ? input.experience : userProfile.experience,
                location: input.location ? input.location : userProfile.location,
                gender: input.gender ? input.gender : userProfile.gender,
                qualification: input.qualification ? input.qualification : userProfile.qualification,
                skill: input.skill ? input.skill : userProfile.qualification
            })
            return next({apiStatus: "SUCCESS", data: input, statusCode: 200})
        } else {
            // let existingSkills = await Skill.find({name: {$in: skill}}).select('_id')
            // for(let i=0; i<existingSkills.length; i++) {
            //     skills.push(existingSkills[i]._id)
            // }
            let newUserProfile = new UserProfile({
                userId: userId,
                firstName,
                lastName,
                age,
                gender,
                location,
                experience,
                qualification,
                skill: skill
            })
            console.log(newUserProfile)

            await newUserProfile.save();
            return next({apiStatus: "SUCCESS", data:newUserProfile, statusCode: 200})
        }
        } catch(err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}

const listProfileDetails = async(req, res, next) => {
    let profileDetails = await UserProfile.findOne({userId: req.user._id})
    return next({apiStatus: "SUCCESS", data:profileDetails, statusCode: 200})
    
}

const editUserProfile = async(req, res, next) => {
    let input = req.body
    try {
        let userProfile = await UserProfile.findOne({userId: req.user._id})
        if(userProfile == null) {
            return next({apiStatus: "FAILURE", errorMessage: "USER_PROFILE_DOESN'T_FOUND", statusCode: 404})
        }
        await userProfile.updateOne({
            firstName: input.firstName ? input.firstName : userProfile.firstName,
            lastName: input.lastName ? input.lastName : userProfile.lastName,
            age: input.age ? input.age : userProfile.age,
            experience: input.experience ? input.experience : userProfile.experience,
            location: input.location ? input.location : userProfile.location,
            gender: input.gender ? input.gender : userProfile.gender,
            qualification: input.qualification ? input.qualification : userProfile.qualification,
        })
        return next({apiStatus:"SUCCESS", data: userProfile, statusCode: 200})
    } catch (err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}



module.exports = {
    createUserProfile,
    editUserProfile,
    listProfileDetails
}
