const Recruiter = require('../models/recruiterSchema')
const Company = require('../models/companySchema')
const Job = require('../models/jobSchema')
const UserProfile = require('../models/userProfileSchema')
const User = require('../models/userSchema')

const addRecruiter = async(req, res, next) => {
    let { company } = req.body
    try {
        if(req.user.role != 'recruiter') {
            return next({apiStatus: "FAILURE", errorMessage: "ACCESS_DENIED", statusCode: 403})
        }
        // let companyExists = await Company.findOne({name: company})
        // console.log(companyExists)
        // if(companyExists == null) {
        //     return next({apiStatus: "FAILURE", errorMessage: "COMPANY_DOESN'T_EXISTS", statusCode: 404})
        // }
        let newRecruiter = new Recruiter({
            userId: req.user._id,
            companyId: company,
            isVerified: false
        })
        await newRecruiter.save();
        return next({apiStatus: "SUCCESS", data: newRecruiter, statusCode: 200})
    } catch(err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}

const approveRecruiter = async(req, res, next) => {
    let { recruiterId } = req.params
    try{
        if(req.user.role != 'owner') {
            return next({apiStatus: "FAILURE", errorMessage: "ACCESS_DENIED", statusCode: 403})
        }
        let recruiter = await Recruiter.findOne({_id: recruiterId}, 'userId companyId isVerified')
        if(recruiter == null) {
            return next({apiStatus: "FAILURE", errorMessage: "RECRUITER_NOT_FOUND", statusCode: 404})
        }
        await recruiter.updateOne({isVerified: true})
        return next({apiStatus: "SUCCESS", data: recruiter, statusCode: 200})
    } catch(err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}

const getPostedJobs = async(req, res, next) => {
    try {
        let recruiter = await Recruiter.findOne({userId: req.user._id})
        if( recruiter == null) {
            return next({apiStatus: "FAILURE", errorMessage: "RECRUITER_DOESN'T_EXISTS", statusCode: 404})
        }
        let jobs = await Job.find({recruiterId: recruiter._id})
        return next({apiStatus: "SUCCESS", data: jobs, statusCode: 200})
    } catch(err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}

const request = async(req, res, next) => {
    let {companyId} = req.params
    console.log(companyId)
    let requestList = []
    let requests = await Recruiter.find({companyId, isVerified: false})
    console.log(requests)
    for(let i=0; i<requests.length; i++) {
        let userProfiles = await UserProfile.findOne({userId: requests[i].userId})
        let user = await User.findOne({_id: userProfiles.userId})
        // console.log(user)
        let details = {
            id: requests[i]._id,
            username: user.username,
            name: userProfiles.firstName + userProfiles.lastName,
            gender: userProfiles.gender,
            experience: userProfiles.experience,
            qualification: userProfiles.qualification
        }
        requestList.push(details)
    }
    return next({apiStatus: "SUCCESS", data: requestList, statusCode: 200})
}

const recruiters = async(req, res, next) => {
    let {companyId} = req.params
    console.log(companyId)
    let requests = await Recruiter.find({companyId, isVerified: true})
    let requestList = []
    console.log(requests)
    for(let i=0; i<requests.length; i++) {
        let userProfiles = await UserProfile.findOne({userId: requests[i].userId})
        let user = await User.findOne({_id: userProfiles.userId})
        // console.log(user)
        let details = {
            id: requests[i]._id,
            username: user.username,
            name: userProfiles.firstName + userProfiles.lastName,
            gender: userProfiles.gender,
            experience: userProfiles.experience,
            qualification: userProfiles.qualification
        }
        requestList.push(details)
    }
    return next({apiStatus: "SUCCESS", data: requestList, statusCode: 200})
}
module.exports = {
    addRecruiter,
    approveRecruiter,
    getPostedJobs,
    request,
    recruiters
}