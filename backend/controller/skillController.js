const Skill = require("../models/skillSchema")

const addSkill = async(req, res, next) => {
    let { name } = req.body
    try {
        let skill = await Skill.findOne({name: name})
        if(skill != null) {
            return next({apiStatus: "FAILURE", errorMessage: "SKILL_ALREADY_EXISTS", statusCode: 409})
        }
        let newSkill = new Skill({
            name: name
        })
        await newSkill.save()
        return next({apiStatus: "SUCCESS", data: newSkill, statusCode: 200})
    } catch(err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}

const getSkills = async(req, res, next) => {
    let { skill } = req.body
    try {
        let skills = await Skill.find({_id: {$in: skill}})
        return next({apiStatus: "SUCCESS", data: skills, statusCode: 200})
    } catch(err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}

const listSkills = async(req, res, next) => {
    let skills = await Skill.find({}).select('name')
    return next({apiStatus: "SUCCESS", data: skills, statusCode: 200})

}

module.exports = {
    addSkill,
    getSkills,
    listSkills
}