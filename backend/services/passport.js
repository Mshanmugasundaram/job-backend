const passport = require('passport');
const jwtStrategy = require('passport-jwt').Strategy,
	ExtractJwt = require('passport-jwt').ExtractJwt;
const LocalStrategy = require('passport-local');
const bcrypt = require('bcryptjs');
require('dotenv').config();

const User = require('../models/userSchema');

const jwtOptions = {
	jwtFromRequest: ExtractJwt.fromExtractors([
		ExtractJwt.fromAuthHeaderWithScheme('JWT'),
		ExtractJwt.fromUrlQueryParameter('token'),
	]),
	secretOrKey: process.env.SECRET_KEY,
};

const customOption = {
	usernameField: 'email',
	passwordField: 'password',
};

const userLogin = new LocalStrategy(customOption, async function (username, password, done) {
	const user = await User.findOne({ email: username });
	console.log(user);
	if (!user) {
		return done(null, false);
	}
	bcrypt.compare(password, user.password, function (err, isMatch) {
		if (err) return done(err, false);
		if (!isMatch) return done(null, false);
		let userDetails = user._doc;
		return done(null, { ...userDetails });
	});
});

const tokenVerification = new jwtStrategy(jwtOptions, async function (payload, done) {
	const user = await User.findOne({ _id: payload.id });
	if (!user) {	
		return done(null, false);
	}
	let userDetails = user._doc;
	// console.log(userDetails)

	return done(null, { ...userDetails });
});

passport.use('userLogin', userLogin);
passport.use('tokenVerification', tokenVerification);

module.exports = passport;
