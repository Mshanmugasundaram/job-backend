var mongoose = require("mongoose");

const MONGO_USERNAME = process.env.DATABASE_USERNAME;
const MONGO_PASSWORD = process.env.DATABASE_PASSWORD;
const MONGO_HOSTNAME = process.env.DATABASE_HOSTNAME;
const MONGO_PORT = process.env.DATABASE_PORT;
const MONGO_DB = process.env.DATABASE_DB;
const url = `mongodb://${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}?authSource=admin`;
mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });


/**
 * mongodb://nodejsdev:keviz%40nodejs@159.65.155.208:27017/connectorder?authSource=admin
 */
