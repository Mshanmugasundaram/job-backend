const express = require("express");
const router = express.Router();
const passport = require("passport");
const { getSkills, addSkill, listSkills } = require("../controller/skillController");


router.get("/get", getSkills);
router.post('/add', addSkill);
router.get('/list', listSkills)

module.exports = router