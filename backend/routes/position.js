const express = require("express");
const router = express.Router();
const passport = require("passport");
const { addPosition } = require("../controller/positionController");


router.post('/add', passport.authenticate("tokenVerification", {session: false}), addPosition)


module.exports = router