const express = require('express')
const router = express.Router()
const passport = require('passport')

const { addJob, listJobs, selectJob, applyJob, applied, editJob } = require('../controller/jobController')

router.get('/applied', passport.authenticate('tokenVerification', {session: false}), applied)
router.get('/', passport.authenticate('tokenVerification', {session: false}), listJobs)
router.get('/:jobId', passport.authenticate('tokenVerification', {session: false}), selectJob)
router.put('/apply/:jobId', applyJob),
router.post('/add', passport.authenticate('tokenVerification', {session: false}), addJob)
router.put('/edit/:jobId', passport.authenticate('tokenVerification', {session: false}), editJob)


module.exports = router