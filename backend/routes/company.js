const express = require("express")
const router = express.Router();
const passport = require("passport");


const {addCompany, recruiterList, companyDetails, companies} = require('../controller/companyController')

router.get('/', passport.authenticate("tokenVerification", {session: false}), companyDetails)
router.get('/all', passport.authenticate("tokenVerification", {session: false}), companies)
router.post('/add', passport.authenticate("tokenVerification", {session: false}), addCompany)
router.get('/recruiter/list', passport.authenticate('tokenVerification', {session: false}), recruiterList)


module.exports = router