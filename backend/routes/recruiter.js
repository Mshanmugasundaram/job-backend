const express = require('express')
const router = express.Router()
const passport = require('passport')
const { addRecruiter, approveRecruiter, getPostedJobs, request, recruiters } = require('../controller/recruiterController')

router.post("/add", passport.authenticate('tokenVerification', {session: false}), addRecruiter);
router.put('/approve/:recruiterId', passport.authenticate("tokenVerification", {session: false}), approveRecruiter);
router.get('/posted/jobs', passport.authenticate('tokenVerification', {session: false}), getPostedJobs);
router.get('/request/:companyId', passport.authenticate('tokenVerification', {session: false}), request);
router.get('/list/:companyId', passport.authenticate('tokenVerification', {session: false}), recruiters)

module.exports = router