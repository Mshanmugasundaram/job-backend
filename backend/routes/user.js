const express = require("express");
const router = express.Router();
const passport = require("passport")

const { signUp, login, forgetPassword } = require("../controller/userController");

router.post("/signup", signUp)
router.post("/", passport.authenticate("userLogin", { session: false }), login)
router.put('/forgetpassword', forgetPassword)


module.exports = router