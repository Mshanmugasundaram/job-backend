const express = require("express");
const router = express.Router();

const userRouter = require("./user");
const userProfileRouter = require("./userProfile")
const skillRouter = require("./skill")
const positionRouter = require("./position")
const companyRouter = require("./company")
const recruiterRouter = require('./recruiter')
const jobRouter = require('./job')

module.exports = (app) => {
    app.use("/user", userRouter)
    app.use("/profile", userProfileRouter)
    app.use("/skill", skillRouter)
    app.use("/position", positionRouter)
    app.use('/company', companyRouter)
    app.use('/recruiter', recruiterRouter)
    app.use('/job', jobRouter)
};