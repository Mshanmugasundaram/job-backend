const express = require("express");
const router = express.Router();
const passport = require("passport");


const { createUserProfile, editUserProfile, listProfileDetails,  } = require("../controller/userProfileController");

router.get("/", passport.authenticate("tokenVerification", { session: false }), listProfileDetails)
router.post("/create",  createUserProfile)
router.put("/edit", passport.authenticate("tokenVerification", { session: false }), editUserProfile)


module.exports = router