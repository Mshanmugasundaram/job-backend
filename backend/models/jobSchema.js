const mongoose = require('mongoose')

const Job = new mongoose.Schema({
    companyId: {
        type: String,
        required: true
    },
    recruiterId: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    skills: {
        type: Array,
        required: true
    },
    location: {
        type: Array,
        required: true
    },
    experience: {
        type: String,
        required: true
    },
    salary: {
        type: Number,
        required: true
    },
    industry: {
        type: String,
        required: true
    }
})

mongoose.model("job", Job)
module.exports = mongoose.model("job")