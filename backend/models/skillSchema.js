const mongoose = require("mongoose")

let Skill = new mongoose.Schema({
    name: {
        type: String,
        required: true
    }
}, {
    timestamps: true
})

mongoose.model("skill", Skill)
module.exports = mongoose.model("skill")