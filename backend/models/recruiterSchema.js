const mongoose = require("mongoose")

let Recruiter = new mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    companyId: {
        type: String,
        required: true
    },
    isVerified: {
        type: Boolean,
        required: true
    }
}, {
    timestamps: true
})

mongoose.model("recruiter", Recruiter)
module.exports = mongoose.model("recruiter")