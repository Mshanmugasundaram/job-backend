const mongoose = require("mongoose")

let Company = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    owner: {
        type: String,
        required: true
    },
    type: {
        type: Array,
        required: true
    }
}, {
    timestamps: true
})

mongoose.model("company", Company)
module.exports = mongoose.model("company")