const mongoose = require("mongoose")

let CompanyProfile = new mongoose.Schema({
    companyId: {
        type: String,
        required: true
    }
}, {
    timestamps: true
})

mongoose.model("companyprofile", CompanyProfile)
module.exports = mongoose.model("companyprofile")