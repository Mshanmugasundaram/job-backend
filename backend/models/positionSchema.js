const mongoose = require("mongoose")

let Position = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    companyId: {
        type: String,
        required: true
    },
    createdBy: {
        type: String,
        required: true
    }
}, {
    timestamps: true
})

mongoose.model("position", Position)
module.exports = mongoose.model("position")