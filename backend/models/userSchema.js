const mongoose = require("mongoose")

let User = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    username:{
        type: String,
        required: true
    },
    password:{
        type: String,
        required: true
    },
    role:{
        type: String,
        enum: ['jobseeker','recruiter', 'owner'],
    }
}, {
    timestamps: true
})

mongoose.model("users", User)
module.exports = mongoose.model("users")