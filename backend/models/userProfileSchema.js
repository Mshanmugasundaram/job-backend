const mongoose = require("mongoose")

let UserProfile = new mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    firstName:{
        type: String,
        required: true
    },
    lastName:{
        type: String,
        required: true
    },
    location:{
        type: String,
        required: true
    },
    experience:{
        type: String,
        required: true
    },
    age:{
        type: Number,
        required: true
    },
    qualification: {
        type: String,
        required: true
    },
    gender:{
        type: String,
        enum: ['male', 'female', 'other'],
        required: true
    },
    skill: {
        type: Array,
        required: true
    },
    jobs: {
        type: Array,
        required: true
    }
}, {
    timestamps: true
})

mongoose.model("userprofile", UserProfile)
module.exports = mongoose.model("userprofile")